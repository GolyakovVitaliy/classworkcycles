import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ClassWork {

    private BufferedReader reader;
    public void bufferMax() {
        reader = new BufferedReader(new InputStreamReader(System.in));
        try {
            System.out.println("Введите А, В, C, D");
            int numA = Integer.parseInt(reader.readLine());
            int numB = Integer.parseInt(reader.readLine());
            int numC = Integer.parseInt(reader.readLine());
            int numD = Integer.parseInt(reader.readLine());
            System.out.println(max(numA, numB, numC, numD));

        } catch(NumberFormatException n) {
            System.out.println("Введите число");
            bufferMax();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String max(int numA, int numB, int numC, int numD) {
        int max1=0;
        int max2=0;
        if (numA > numB) {max1=numA;}
        if (numA < numB) {max1=numB;}
        if (numC > numD) {max2=numC;}
        if (numC < numD) {max2=numD;}

        if (max1 > max2) { return ("Максимум = "+max1);}
        else { return ("Максимум = "+max2);}
    }

    public void Cycles() {
        //В цикле вывести в консоль от A до Z
        System.out.println("->В цикле вывести в консоль от A до Z");
        for (char i = 'A'; i <= 'Z'; i++) {
            System.out.print(i + " ");
        }
        //В цикле вывести в консоль от Z до A
        System.out.println();
        System.out.println("В цикле вывести в консоль от Z до A");
        for (char i = 'Z'; i >= 'A'; i--) {
            System.out.print(i + " ");
        }
        //В цикле вывести в консоль от А до Я
        System.out.println();
        System.out.println("->В цикле вывести в консоль от А до Я");
        for (char i = 'А'; i <= 'Я'; i++) {
            System.out.print(i + " ");
        }
        //В цикле вывести в консоль от я до б
        System.out.println();
        System.out.println("->В цикле вывести в консоль от я до б");
        for (char i = 'я'; i >= 'б'; i--) {
            System.out.print(i + " ");
        }
        //В цикле вывести в консоль от 0 до 90
        System.out.println();
        int x=0;
        System.out.println("->В цикле вывести в консоль от 0 до 90");
        for (int i = 0; i <= 90; i++) {
            System.out.print(i + " ");
            x++;
            if (x==31) {
                System.out.println();
                x=1;
            }
        }
        //В цикле вывести в консоль от 90 до 0
        System.out.println();
        x=0;
        System.out.println("->В цикле вывести в консоль от 90 до 0");
        for (int i = 90; i >= 0; i--) {
            System.out.print(i + " ");
            x++;
            if (x==31) {
                System.out.println();
                x=1;
            }
        }
        //В цикле вывести в консоль от -10 до 20
        System.out.println();
        System.out.println("->В цикле вывести в консоль от -10 до 20");
        for (int i = -10; i <= 20; i++) {
            System.out.print(i + " ");
        }
        //Выведите в консоль index от 0 до 20
        System.out.println();
        System.out.println("->Выведите в консоль index от 0 до 20");
        int index = 0;
        x=0;
        while (index <= 20) {
            System.out.print(index+" ");
            index++;
        }
        //Используйте переменную index в do while и выведите "do while + index"
        System.out.println();
        index=0;
        x=0;
        System.out.println("->Используйте переменную index в do while и выведите \"do while + index\"");
        do {
            System.out.print(" do while " + index);
            index++;
            x++;
            if (x==11) {
                System.out.println();
                x=0;
            }
        }
        while (index <= 20);
        //Квадрат
        System.out.println();
        System.out.println("->Квадрат");
        for (int i = 0; i <= 5; i++) {
            for (int j = 0; j <= 5; j++) {
                if (i > 0 && j < 5 && i < 5 && j > 0) {
                    System.out.print("   ");
                } else System.out.print(" * ");
            }

            System.out.println();
        }

    }
}
