import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Calc {

    private BufferedReader reader;
    public void bufferoper() {
        reader = new BufferedReader(new InputStreamReader(System.in));
        try {
            System.out.println("Введите операцию");
            String oper = reader.readLine();

            switch (oper) {
                case "help":
                    System.out.println("+ Сложение A и B");
                    System.out.println("- Вычитание B из A");
                    System.out.println("* Умножение A на B");
                    System.out.println("/ Деление A на B");
                    System.out.println("% Деление по модулю A на B");
                    System.out.println("^ Возведение A в степень B");
                    System.out.println("root Вычисление из А корня степени B");
                    System.out.println("rand Вывод случайного числа от A до B");
                    System.out.println("help Вывод списка операций");
                    System.out.println("exit Выход из программы");
                    System.out.println("Введите операцию");
                    break;
                case "exit":
                    System.exit(0);
                    break;
                case "+": ;
                    break;
                case "-": ;
                    break;
                case "*": ;
                    break;
                case "/": ;
                    break;
                case "%": ;
                    break;
                case "^": ;
                    break;
                case "root": ;
                    break;
                case "rand": ;
                    break;
               default:
                    System.out.println("Операция не распознана. Повторите ввод");
                   bufferoper();
                    break;
            }

            System.out.println("Введите 2 значения");
            int numA = Integer.parseInt(reader.readLine());
            int numB = Integer.parseInt(reader.readLine());
            System.out.println(operacja(oper, numA, numB));

        } catch(NumberFormatException n) {
            System.out.println("Нужно ввести число");
            bufferoper();
        }
        catch (IOException e) {
            e.printStackTrace();
        }

    }

    private double operacja(String oper, double numA, double numB) {
        double res=0;
        switch (oper) {
            case "+": res= (numA + numB);
                break;
            case "-": res= (numA - numB);
                break;
            case "*": res= (numA * numB);
                break;
            case "/": res= (numA / numB);
                break;
            case "%": res= (numA % numB);
                break;
            case "^": res= (Math.pow(numA, numB));
                break;
            case "root": res= Math.pow( numA, 1.0 / numB );
                break;
            case "rand": res=(float)(Math.random()+numB-1);
                break;
            case "exit": System.exit(0);
                break;
            //default:
           // System.out.println("Операция не распознана. Повторите ввод");

        }
        return res;
    }
}
