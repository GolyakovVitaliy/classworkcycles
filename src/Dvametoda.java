public class Dvametoda {

    public void Chet() {

        for (int i = 0; i <= 40; i++) {
            if (i % 2 == 0) {
                System.out.print(i + " ");
            }
        }
        System.out.println();
    }

    public void Nechet() {

        for (int i = 0; i <= 40; i++) {
            if (i % 2 != 0) {
                System.out.print(i + " ");
            }
        }
    }
}